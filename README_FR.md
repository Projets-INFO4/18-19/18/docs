# docs

Documentation du projet  "Générateur de tableau de bord pour applications scientifiques"  
  
## Journal de bord

### Semaine 0 - du 21 au 27 janvier 2019
Sélection du sujet de projet  
Découverte rapide et installation des outils à utiliser

### Semaine 1 - du 28 janvier au 3 février 2019
Prise en main des outils Dashboard et PyYaml  
Réflexion quant à la conception-même de l'outil :  
- qu'attendons-nous en entrées ?  
- que fournissons-nous en sortie ?  
- quel visuel global ? (affichage du graphique, etc)  
- définition d'un process d'utilisation à potentiellement mettre en place : 
    - entrée d'un fichier à l'aide d'une page formulaire
    - analyse du fichier (récupération des plages de données, etc)
    - choix de ce qui devra être affiché (abscisses, ordonnées, combien de graphes, types des graphes, etc) et de comment l'afficher (template YAML + CSS)
    - page d'affichage générée à partir des choix faits par l'utilisateur
    
### Semaine 2 - du 4 au 10 février 2019
Réalisation des premiers prototypes babySteps : 
- analyse d'un fichier .CSV simple, récupération des données et affichage d'un tableau et d'un graphique
- création d'un template simple dans un .YAML qui s'affiche effectivement
→ ces prototypes sont accessibles dans le projet ***prototypes***, branche *proto1*

### Semaine 3 - du 11 au 17 février 2019
Mini démonstration des prototypes avec Mr Richard → redéfinition du travail à effectuer (expressions régulières + point de vue utilisateur)  
Mise au point sur les expressions régulières  
Codage d'un programme qui prend en entrée un fichier texte et une expression régulière et qui donne en sortie le résultat de la recherche par ER → cela nous permet
de récupérer des plages de données que l'on peut ensuite traiter (affichage du graphique, etc)  

### Semaine 4 - du 18 au 24 février 2019
Réalisation d'un nouveau prototype *Prototype4.py* combinant *Prototype3.py* et *PrototypeExpReg.py*  
Pistes d'amélioration après présentation et discussion avec Mr Richard :
- ne plus avoir recourt à un fichier temporaire lors du traitement des données → OK
- modifier pour ne plus avoir besoin de l'appel à *PrototypeExpReg.py* → OK
- réfléchir aux cas concernant des gros fichiers en lecture (il ne faut pas que cela prenne des heures, threading ?) → amélioration grâce à la suppression du
fichier temporaire (?)
- concernant les expressions régulières, réfléchir à la prise en compte des erreurs + travailler avec les groupements (par parenthésage)  

Sources d'erreur sur lesquelles se pencher : 
- concernant les jeux de données contenant des nombres à virgule : format numérique anglais, sinon, prendre en compte le fait que le format CSV peut "séparer" les 
nombres à virgules en les entourant de guillemets dactylographiques → pour le moment, on utilise format numérique anglais

### Semaine 5 -  du 4 au 10 mars 2019
Amélioration de *Prototype4.py* :
- prise en argument du séparateur des jeux de données
- début de choix de tracé avec modifications prises en compte en temps réel : l'utilisateur peut choisir quel jeu de données sera posé en abscisse et les autres
jeux restant seront en ordoonée 

Travail sur les expressions régulières et les groupements  
Travail sur un template YAML "modulable"

## Description du travail en cours

### Courtes descriptions des prototypes
- *Prototype1.py* : Permet d'upload un fichier .csv à 2 colonnes. Affiche les données dans un tableau et les représente sur un graphique.
- *Prototype3.py* : Ouvre un fichier .csv donné en dur dans le code, contenant autant de colonnes que souhaité. Le programme représente toutes les colonnes en 
fonction de la première colonne.
- *Prototype1-3.py* : Permet d'upload un fichier .csv, contenant autant de colonnes que souhaité. Le programme représente toutes les colonnes en fonction de la 
première colonne.
- *PrototypeExpReg.py* : Prend un arguments un fichier et une expression régulière. Renvoie le résultat du traitement du fichier par l'expression eégulière.
- *Prototype4.py* : Prend en arguments le fichier à traiter, contenant autant de colonnes que souhaité, et une expression régulière. Le programme traite le fichier 
en fonction de l'expression régulière fournie. Le fichier doit être organisé en colonne. Le séparateur est indiqué en dur dans le code.

### A faire
- Trouver comment faire des groupes avec les expressions régulières
- Faire que l'utilisateur puisse choisir ce qui est tracé en fonction de quoi -> choix des ordonnées à faire
- Faire un template YAML pouvant s'adapter aux informations données par l'utilisateur