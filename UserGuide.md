# User Guide

"Dashboard generator applied to scientific applications" Project's user guide

## Introduction
This project consists of generating a dashboard, usable in many different scientific contexts, from a YAML template indicating which components to use and their respective positions.

## Requiered packages
In order to execute the code of this project without any technical difficulty, some packages must be installed.  
The code is written in Python language. With the actual version of the program, only Python 2 is supported.  

In order to determine the version of Python that is currently used on one's computer, use the command line `python --version`.

To help with the installation of the needed packages, it is advised to install the package management system ***pip*** thanks to the command line: `sudo apt-get install python-pip`.  

The different packages can now be easily installed. Please execute these command lines:  
<pre><code>sudo pip install dash==0.39.0  # The core dash backend
sudo pip install dash-html-components==0.14.0  # HTML components
sudo pip install dash-core-components==0.44.0  # Supercharged components
sudo pip install dash-table==3.6.0  # Interactive DataTable component </code></pre>   

Then, install ***PyYaml*** with this command line:
<pre><code>sudo pip install yaml</code></pre> 

Then, try to install ***regex*** with this command line:  
<pre><code>sudo pip install regex</code></pre>

If it raises an error, do this:  
<pre><code>sudo apt-get install python-dev  \
    build-essential libssl-dev libffi-dev \
    libxml2-dev libxslt1-dev zlib1g-dev \
    python-pip</code></pre>

And then try again:
<pre><code>sudo pip install regex</code></pre>

## Execution of the program
The program takes 3 arguments :
- the file to process  
- the regular expression used to process the file  
- the YAML template

### Input file
* The input file must contain numeric data.  
* Each series of data is represented by a column.  
* There can be as many rows and columns as desired.  
* The first line treated will be considered as the names of the columns (so as the name of the data series).  
* On each line, the data in each column is delimited by a string separator. This separator must be included is the regular expression.  
* It is possible to write comment lines by starting the line with a character ignored by the supplied regular expression.  
* If a regular expression matches a string, the regular expression will return the whole line. In this project, it is mandatory to use what is called "groups" in the regular expression. 
* Groups define what you want your regular expression to return. Groups are defined using parenthesis "()". 

### Regular Expression
Here is a little help, examples to understand how regular expressions work:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**^** : start of a line  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**$** : end of a line  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[a-z]** : matches lower case letter from a to z  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[A-Z]** : matches upper case letter from A to Z  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[A-P]** : matches upper case letter from A to P  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[0-9]** : matches digit from 0 to 9  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**.** : any kind of character  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**\w** : equals [a-zA-Z0-9_]  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**\d** : equals [0-9], matches a digit  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**\t** : matches a tabulation  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**\n** : matches a carriage return  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**{6}** : quantifier, for exemple [a-zA-Z]{6} matches a string that has a word of 6 letters  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**abc{2,}** : matches a string that has ab follow by 2 c or more  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**abc{3,4}** : matches a string that has ab follow by 3 up to 4 c  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**a|b** : matchs the character a or the character b  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[ab]** : same as previously  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**abc** : matches a string that has the word abc in it  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**abc*** : matches a string that has ab followed by zero or more c  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**abc+** : matches a string that has ab followed by one or more c  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**[^a-zA-Z]** : matches a string that has NOT a letter from a to z in upper or lower case  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**([a-z])a([a-z])** : matches a string witch got a letter from a to z at the beginning then an a and finally a letter from a to z again. It will return a list composed of 2 things: the first and the last character of the matched string.    


For special symbols like **?** or **!** or **.**, it is compulsory to write "\\" before it. For example, in order to match a line beginning with a ? : **^\\?**  

An important point to understand is that you can specify with the regular expression the column separator that is used in your data file. For example, in a .csv file, the separator is ",". But you can use the character that you want to separate the columns.  
Note that to use the space as a separator, you must use **"\s"**.

Here are samples of regular expressions that can be used :   
**^([0-9])([0-9])** : Takes into account every line starting with 2 digits.  
**^[a-zA-Z0-9]\*\s[a-zA-Z0-9]\*\s([a-zA-Z0-9]\*)\s([a-zA-Z0-9]\*)** : Takes into account every line where there are 4 columns at least, separate with a space character, and match only the 3rd and 4th column.  
**^([a-zA-Z0-9]\*)\s([a-zA-Z0-9]\*)** : Takes into account every line where there are 2 columns at least, separate with a space character, and keeps only the 1st and 2nd columns.  
**^([a-zA-Z0-9]\*),((?1))** : Same as the previous line, but in a more simplificated version, and with a comma as a separator.

### YAML Template
**The YAML Template describes what component you want to display in your dashboard, and where these components should be display.**

* The HTML page of the dashboard is organised as a tree.  
* The tree **MUST begin** with a ***div***, which is the root. All component must be added **INSIDE** this first div.  
* You can add more *div* inside the root of your template. If you add more than one *div* in the same *div*, you must differenciate them by using *div1*, *div2*...  
* You MUST use one time, and ONLY ONE TIME, the keywords ***graph*** and ***options*** in your template.  
* You can add comments in your YAML Template by using the character # at the beginning of each line intended to be a comment.  
* You must put ":" after each keyword.

Note that the components added in the root div will be treated from the bottom to the top of your YAML file. In the example below, the program will read *div2(header,options)*, and then *div1(graph,table)*.  

**The available keywords are :**
* ***div*** : Creates an invisible "box" that can contain components (must use div at least one time as the root of the template ; if there are several div at the same level, use div1, div2...)
* ***graph*** : Creates the graph (obligatory, ONE AND ONLY ONE TIME)
* ***options*** : Creates the settings panel (obligatory, ONE AND ONLY ONE TIME)
* ***header*** : Creates a header that display the name of the provided data file and the provided Regular Expression
* ***table*** : Creates a table to display the data extracted from the provided data file with the provided Regular Expression


Here is an example of a YAML Template :
<pre><code>div:
  # LOWER BOX
  div1:
     graph:
     table: # Display data in a table
  # TOP BOX
  div2:
     header:
     options:</pre></code>