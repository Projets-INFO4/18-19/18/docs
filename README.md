# docs

"Dashboard generator applied to scientific applications" Project's documentation  
  
## Logbook

### Week 0 - 21/01 to 27/01
Project selection  
Quick discovery and installation of the different required tools (DashBoard and PyYaml)  

### Week 1 - 28/01 to 03/02
Picking up Dashboard and PyYaml tools  
Brainstorming as for the design of the generator :
- what is expected for the input
- what is given at the output
- what should be the overall imagery
- early defining of a usage process to potentially set up :
    - the user gives a file to the app through a form page
    - the application analyzes the file and filter the useful information (datasets, etc)
    - the user chooses what should be displayed and how (how many graphs, what types of graphs, selection of the abscissa and the ordinates, etc)
    - the final page is then generated from the previous choices made by the user

### Week 2 - 04/02 to 10/02
Realisation of the firsts prototypes :
- scan a simple .CSV file, retrieve the data and display a table and a graph
- creation of a simple template in a .YAML

→ these prototypes can be found in the project ***prototypes***, branch *proto1*

### Week 3 - 11/02 to 17/02
Short demonstration of the early protoypes with M.Richard → redefinition of the work to be done (regular expressions + user point of view)  
Enhancing the group's knowledge about regular expressions  
Coding a program requesting a text file and a regexp for the input and outputting the result of the search by regexp → allows us to retrieve data ranges that can be processed (graph display, etc.)

### Week 4 - 18/02 to 24/02
Realization of a new prototype *Prototype4.py* combining *Prototype3.py* and *PrototypeExpReg.py*
Suggested improvement after presentation and discussion with M.Richard:
- stop using a temporary file when processing data → OK
- avoid calling * PrototypeExpReg.py * → OK
- think about the cases concerning big files in reading (it does not have to take hours, threading?) → improvement thanks to the suppression of the temporary file (?)
- concerning regular expressions, think about taking into account errors + working with groupings (using parenthesis in regexp)

Sources of error to consider:
- concerning datasets containing decimal numbers: English numeric format, otherwise, take into account the fact that the CSV format can "separate" the numbers with double quotation marks → for the moment, we use English numeric format

### Week 5 - 04/03 to 10/03
Improvement of *Prototype5.py*:
- the separator of the datasets is now taken as argument of the function
- the user can now make its plot choices in real time : the user can choose which dataset will be placed on the abscissa and the others
remaining games will be in ordinates → **EDIT** (05/03), the user can now choose which dataset(s) to put in ordinate, and adjust the size of the markers on the graph

Work on regexp and groupings:
- **EDIT** (05/03) the program is now able to process only the colums of dataset selected by the regexp

Work on a "flexible" YAML template (branch ***yaml_test***):
- **EDIT** (05/03) it is now clear how to adapt the content of the YAML template with the information given by the user (cf the title)
    - with *generate_template.py*, it is possible to write a layout and convert it into a .YAML file.

### Week 6 - 11/03 to 17/03
Improvement of *generate_template.py* leading to the creation of *Prototype6.py* (branch *yaml_tests*) : by rewriting the code of *Prototype5.py*, it seems like we have a potential prototype answering all the expectations, in a quite basic way  
Creation of *ExpPrototype5.py* : improvement of *Prototye5.py* in order to implements regexp grouping  
Modification of *Protoype5.py* : adding new callbacks to specify new options (take it into account for *Prototype6.py*)  
Preparation for mid-project presentation.

### Week 7 - 18/03 to 24/03
Finishing the mid-project presentation  
Oral presentation in front of M.Richard, M.Palix and M.Donsez → induced a massive merge on branch *master*  
Cleaning of the git repository → at the present time, *Prototype5.py* is the functional version of the program  
Creation of two new branches :
- *new_yaml* : trying to implement the feature to execute the program with a simple YAML template, generating a dictionary to build the tree representing the HTML page.
- *RealTime* : trying to enhance the actual program by allowing it to take as an argument a data flow that evolves in real time

Mail exchanges with M.Richard :
- *new_yaml* : a solution may be to interprete (and not toString and parse) the dictionary created from the loading of the .YAML file + (optional) implementing a cache for the HTML pages creation
- *RealTime* : a simple implementation could be made thanks to two parameters : refreshing time, limiting the number of lines of the file the program can get (cf cli :  *tail -n* )

### Week 8 - 25/03 to 31/03
Discussion around the sequence diagram with M.Richard :
- the user should use a URL link instead of the program → induced the creation of the branch *cgi_server* where is studied the possibility of using a CGI Python server
- the construction of the layout should be cached in order to save some time → idem *cgi_server*
- there should be a periodic refreshment of the page, especially when it comes to real time datasets
- we should think about some kind of separation between client and server

Amelioration of the construction of the layout thanks to the .YAML template → DONE! Now the YAML Template is really easy to write, so the user can write his own templates!  
*Prototype7.py* is now the new most advanced deliverable version of our project.  

### Week 9 - 01/04 to 07/04
Reviewing *Prototype7.py* by preventing any crash if the regular expression processing returns a void dataframe + last debugging + last comments  
Writing a bug report about the refreshment feature colliding with Dash integrated features. This bug report is available as appendix 2 in the final report.  
Finalization of the User Guide.  
Writing of the project's final report.  
Preparation of the slides for the final oral presentation

## Protypes' short reviews
- *Prototype1.py* : Allows uploading a 2-column .CSV file. Displays the data in a table and represents it on a chart.
- *Prototype3.py* : Opens .CSV file that was directly given in the code, containing as n columns. Represents all columns in accordance with the first column.
- *Prototype1-3.py* :  Allows uploading a n-column .CSV file. Represents all columns in accordance with the first column.
- *PrototypeExpReg.py* : Takes a file and a regexp. Outputs the result of the file processed by the regexp.
- *Prototype4.py* : Takes the file to be processed, containing as many columns as desired, and a regexp for the input. Processes the file depending on the regexpprovided. The file must be organized in columns. The separator is fixed directly in the code.
- *Prototype5.py* : Takes the file to be processed, containing as many columns as desired, a regexp for the input and the desired separtor of the datasets. Processes the file depending on the regexpprovided. The file must be organized in columns.
- *generate_yaml.py* : Creates a blank .YAML template, which has to be completed in the main program.
- *Prototype6.py* : Same as *Prototype5.py*, but takes as an extra argument a specific .YAML template to complete.
- *Prototype7.py* : Same as *Prototype5.py*, but takes as an extra argument a specific .YAML template to complete + allows real time modification of the data sets
